//Erik Bissell
// Lab 6
//input number of rows
//output PatternB

import java.util.Scanner;

public class PatternB
{
  public static void main( String [] args)
  {
    Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter the number of rows you would like 1-10");
		
		int numberOfRows = scan.nextInt();
		
		//will loop until user puts in a number between 1-10
		while(numberOfRows >= 10 || numberOfRows <= 1)
		{
			System.out.println(" Please try again. How many rows would you like 1-10");
			numberOfRows = scan.nextInt();
		}
    
    //outside for loop dictates the number of rows based off of user input
    //decreasing to 1 because pattern B decreases with each row 
    for(int i = numberOfRows; i >= 0; i--)
		{
      // nested for loops prints out j increasing until j = i
			for( int j = 1; j<=i; j++)
			{
				System.out.print(j + " ");
			}
			System.out.println(" ");
		}
		
    
    
  }
}
