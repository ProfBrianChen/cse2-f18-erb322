//Erik Bissell
// Lab 6
//input number of rows
//output PatternD

import java.util.Scanner;
public class PatternD
{
  public static void main( String [] args)
  {
    Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter the number of rows you would like 1-10");
		
		int numberOfRows = scan.nextInt();
		
		//will loop until user puts in a number between 1-10
		while(numberOfRows >= 10 || numberOfRows <= 1)
		{
			System.out.println(" Please try again. How many rows would you like 1-10");
			numberOfRows = scan.nextInt();
		}
        
    //outside for loop dictates the number of rows based off of user input
    //need to start at highest number because j starts at i 
    for( int i = numberOfRows; i>0; i-- )
		{
      //nested for loop starts at i and subtracts so the numbers print in descending order 
			for( int j = i; j>0; j--)
			{
				System.out.print(j + " ");
			}
			System.out.println(" ");
		}
    
    
  }
}
