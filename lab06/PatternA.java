//Erik Bissell
// Lab 6
//input number of rows
//output PatternA

import java.util.Scanner;

public class PatternA
{
  public static void main( String [] args)
  {
    Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter the number of rows you would like 1-10");
		
		int numberOfRows = scan.nextInt();
		
		//will loop until user puts in a number between 1-10
		while(numberOfRows >= 10 || numberOfRows <= 1)
		{
			System.out.println(" Please try again. How many rows would you like 1-10");
			numberOfRows = scan.nextInt();
		}
    //outside for loop dictates the number of rows based off of user input
		for(int i = 1; i <= numberOfRows; i++)
		{
      //nested for loop will print out numbers increasing by one with a space until it reaches i for each loop 
			for(int j = 1; j <= i; j++)
			{
				System.out.print(j + " ");
			}
      //next loop ends up on the next line
			System.out.println(" ");
		}
    
  }
}