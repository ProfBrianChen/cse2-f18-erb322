//Erik Bissell
// Lab 6
//input number of rows
//output PatternC

import java.util.Scanner;
public class PatternC
{
  public static void main( String [] args)
  {
    Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter the number of rows you would like 1-10");
		
		int numberOfRows = scan.nextInt();
		
			//will loop until user puts in a number between 1-10
		while(numberOfRows >= 10 || numberOfRows <= 1)
		{
			System.out.println(" Please try again. How many rows would you like 1-10");
			numberOfRows = scan.nextInt();
		}
    
    //outside for loop dictates the number of rows based off of user input
    for(int i = 1; i <= numberOfRows; i++)
		{
		  //this nested for loop will right align all of the numbers 
			for( int j = 1; j <= (numberOfRows - i); j++)
			{
				System.out.print(" ");
			}
      //this nested for loop prints out the numbers from highest to lowest, so it starts at i
      //subtracts 1 each iteration
			for( int j = i; j > 0; j--)
			{
				System.out.print(j);
			}
			System.out.println(" ");
		}
    
  }
}
