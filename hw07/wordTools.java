//Erik Bissell
//CSE 002-110
//hw 07


import java.util.Scanner;


public class wordTools {

	public static int findText(String text, String word)
	{
		//im not too sure how to implement this one tbh
		int foundWord = 0;
		boolean found = false;
		for(int i = 0; i < text.length() - (word.length()); i++)
		{
			for (int j = 0; j<word.length() -1; j++)
			{
				
				
				
			}
		}
		
		return foundWord;
	}
	//inputs the sample text, if there are 2+ spaces in a row it shortens it to 1 space
	public static String shortenSpace(String input)
	{
		String shortenedSpaces = "";
		for(int i = 0; i < input.length() -1; i++)
		{
			if(input.charAt(i) == ' ' && input.charAt(i+1) != ' ')
			{
				shortenedSpaces += input.charAt(i);
			}
		}
		System.out.println(shortenedSpaces);
		return shortenedSpaces;
		
	}
	//searches for ! and replaces them with . 
	public static String replaceExclamation(String input)
	{
		String noExclamation = "";
		for(int i = 0; i<input.length() - 1; i++)
		{
			if(input.charAt(i) == '!')
			{
				noExclamation += ".";
			}
			else
			{
				noExclamation += input.charAt(i);
			}
		}
		
		return noExclamation;
	}
	
	//returns the total number of words
	public static int getNumOfWords(String input)
	{
	int numWords=1;	
		
	for(int i=0; i < input.length()-1; i++)
	{
		if(input.charAt(i)!= ' ' && input.charAt(i+1) == ' ')
		{
			numWords++;
		}
	}
	
	return numWords;
	}
	//returns number of non white space characters
	public static int getNumOfNonWSCharacters(String input)
	{
		int totalNonWSCharacters=0;
		int totalWSCharacters=0;
		for(int i=0; i<input.length()-1; i++)
		{
			if(input.charAt(i)==' ')
			{
				totalWSCharacters++;
			}
			
		}
		totalNonWSCharacters = input.length() - totalWSCharacters;
		
		
		return totalNonWSCharacters;
	}
	
	//gets sample text from the user 
	public static String sampleText()
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a sample text: ");
		String textInput = scan.nextLine();
		System.out.println("You entered: " +textInput);
		
		
		return textInput;
	}
	
	//prints out options that the user has 
	public static char printMenu()
	{
		//print out the menu
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - quit");
		//asks user for what the want
		Scanner scan = new Scanner(System.in);
		System.out.println("choose an option");
		char choice = scan.next().charAt(0);
		
		

		return choice;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String text = sampleText();
		char menuChoice = 'a'; 
		while(menuChoice != 'q') 
		{
			menuChoice = printMenu();
		switch(menuChoice)
		{
		case 'c':
			System.out.println("Number of non White Space characters: " + getNumOfNonWSCharacters(text));
			//non white space characters 
			break;
		case 'w': 
			System.out.println("Number of words: " + getNumOfWords(text));//number of words
			break;
		case 'f': 
			System.out.println("what word would you like to search for");
			Scanner scan = new Scanner(System.in);
			String word = scan.nextLine();
			System.out.println(word + "occurs: " + findText(text,word) + " times.");
			//find text
			break;
		case 'r': 
			System.out.println(replaceExclamation(text));//replace all !
			break;
		case 'q': //quit 
			break;
		case 's':
			System.out.println(shortenSpace(text));
			//shortenSpaces if more than 2 in a row
			break;
		default: System.out.println("I dont seem to recognize that command");
			break;
			
		}
		
		}
	}

}
