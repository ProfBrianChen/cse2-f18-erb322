import java.util.Scanner;

public class CrapsSwitch {

	public static void main(String[] args) {

     System.out.println("Enter 'random' if you would like to randomly roll the dice");
	    System.out.println("Enter 'state' if you'd like to state what the dice will be");
	   
	    Scanner scan = new Scanner(System.in);
	    //assigns randomOrState depending on if the user wants random dice or if they should choose
	    String randomOrState = scan.nextLine();
	    
	    int die1 = 0;
	    int die2 = 0;
	    
	    if(randomOrState.equalsIgnoreCase("state"))
	    {
	      System.out.println("Enter what you would like the first die to equal 1-6");
	    
	      do
	     {
	    	  //gets value of die1
	      die1 = scan.nextInt();
	      //keeps looking for a valid input for die1 from 1-6
	     } while((die1 > 6 || die1 < 1));
	      
	      System.out.println("Enter what you would like the second die to equal 1-6");
		    
	      do
	     {
	    	  //gets value of die2
	      die2 = scan.nextInt();
	      //keeps looking for a valid input for die2 from 1-6
	     } while((die2 > 6 || die2 < 1));
	 
	    }
	    else if(randomOrState.equalsIgnoreCase("random"))
	    {
	    	//assigns die1 and die2 to random numbers 1-6
	    	die1=(int)(Math.random()*(6))+1;
	    	die2=(int)(Math.random()*(6))+1;	
	    }
	    
	    else
	    {
	    	System.out.println("there was an error you didn't input 'state' or 'random' ");
	    }
	    
		System.out.println(" ");

		String slang=" ";
		
		switch(die1)
		{
		case 1: 
			switch(die2)
			{
			case 1: slang = "Snake Eyes";
			break;
			case 2: slang = "Ace Deuce";
			break;
			case 3: slang = "Easy Four";
			break;
			case 4: slang = "Fever Five";
			break;
			case 5: slang = "Easy Six";
			break;
			case 6: slang = "Seven Out";
			break;
			}
			break;
		case 2: 
			switch(die2)
			{
			case 1: slang = "Ace Deuce";
			break;
			case 2: slang = "Hard Four";
			break;
			case 3: slang = "Fever Five";
			break;
			case 4: slang = "Easy Six";
			break;
			case 5: slang = "Seven Out";
			break;
			case 6: slang = "Easy Eight";
			break;
			}
			break;
		case 3:
			switch(die2)
			{
			case 1: slang = "Easy Four";
			break;
			case 2: slang = "Fever Five";
			break;
			case 3: slang = "Hard Six";
			break;
			case 4: slang = "Seven Out";
			break;
			case 5: slang = "Easy Eight";
			break;
			case 6: slang = "Nine";
			break;
			}
			break;
		case 4:
			switch(die2)
			{
			case 1: slang = "Fever Five";
			break;
			case 2: slang = "Easy Six";
			break;
			case 3: slang = "Seven Out";
			break;
			case 4: slang = "Hard Eight";
			break;
			case 5: slang = "Nine";
			break;
			case 6: slang = "Easy Ten";
			break;
			}
			break;
		case 5:
			switch(die2)
			{
			case 1: slang = "Easy Six";
			break;
			case 2: slang = "Seven Out";
			break;
			case 3: slang = "Easy Eight";
			break;
			case 4: slang = "Nine";
			break;
			case 5: slang = "Hard Ten";
			break;
			case 6: slang = "Yo-leven";
			break;
			}
			break;
		case 6:
			switch(die2)
			{
			case 1: slang = "Seven Out";
			break;
			case 2: slang = "Easy Eight";
			break;
			case 3: slang = "Nine";
			break;
			case 4: slang = "Easy Ten";
			break;
			case 5: slang = "Yo-leven";
			break;
			case 6: slang = "Boxcars";
			break;
			}
			break;
		
			
		}
		System.out.println("Die one is: " + die1 + ". Die two is: " + die2 + ".");
		System.out.println(slang);
    
    
    
  }
}
