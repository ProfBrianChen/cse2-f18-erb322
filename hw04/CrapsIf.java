import java.util.Scanner;

public class CrapsIf {

	public static void main(String[] args) {

      System.out.println("Enter 'random' if you would like to randomly roll the dice");
	    System.out.println("Enter 'state' if you'd like to state what the dice will be");
    
      //creates a scanner named scan 
	   Scanner scan = new Scanner(System.in);
	    //assigns randomOrState depending on if the user wants random dice or if they should choose
	    String randomOrState = scan.nextLine();
	    
	    int die1 = 0;
	    int die2 = 0;
	    
	    if(randomOrState.equalsIgnoreCase("state"))
	    {
	      System.out.println("Enter what you would like the first die to equal 1-6");
	    
	      do
	     {
	    	  //gets value of die1
	      die1 = scan.nextInt();
	      //keeps looking for a valid input for die1 from 1-6
	     } while((die1 > 6 || die1 < 1));
	      
	      System.out.println("Enter what you would like the second die to equal 1-6");
		    
	      do
	     {
	    	  //gets value of die2
	      die2 = scan.nextInt();
	      //keeps looking for a valid input for die2 from 1-6
	     } while((die2 > 6 || die2 < 1));
	 
	    }
	    else if(randomOrState.equalsIgnoreCase("random"))
	    {
	    	//assigns die1 and die2 to random numbers 1-6
	    	die1=(int)(Math.random()*(6))+1;
	    	die2=(int)(Math.random()*(6))+1;	
	    }
	    
	    else
	    {
	    	System.out.println("there was an error you didn't input 'state' or 'random' ");
	    }
	    
		System.out.println(" ");
		
		
		//the following are when both die are the same 
		//both 1
		if(die1 == 1  && die2 == 1)
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Snake eyes!");
		}
		
		//both 2
		if(die1 == 2 && die2 == 2)
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Hard Four!");
		}
		
		//both 3
		if(die1 == 3 && die2 == 3)
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Hard Six");
		}
		
		//both 4
		if(die1 == 4 && die2 == 4)
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Hard Eight!");
		}
		
		//both 5
		if(die1 == 5 && die2 == 5)
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Hard Ten!");
		}
		
		//both6
		if(die1 == 6 && die2 == 6)
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Boxcars!");
		}
		
		//the following are when ther die are different
		//1 and 2
		if(( die1 == 1 || die2 == 1 ) && ( die1 == 2 || die2 == 2 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Ace Deuce!");
		}
		
		//1 and 3
		if(( die1 == 1 || die2 == 1 ) && ( die1 == 3 || die2 == 3 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Easy Four!");
		}

		
		//1 and 4
		if(( die1 == 1 || die2 == 1 ) && ( die1 == 4 || die2 == 4 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Fever 5!");
		}
		
		//1 and 5 
		
		if(( die1 == 1 || die2 == 1 ) && ( die1 == 5 || die2 == 5 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Easy Six!");
		}
		
		//1 and 6
		
		if(( die1 == 1 || die2 == 1 ) && ( die1 == 6 || die2 == 6 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Seven out!");
		}
		
		//2 and 3
		if(( die1 == 2 || die2 == 2 ) && ( die1 == 3 || die2 == 3 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Fever Five!");
		}
		
		//2 and 4
		if(( die1 == 2 || die2 == 2 ) && ( die1 == 4 || die2 == 4 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Easy Six!");
		}
		
		//2 and 5
		if(( die1 == 2 || die2 == 2 ) && ( die1 == 5 || die2 == 5 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Seven Out!");
		}
		
		
		//2 and 6
		if(( die1 == 2 || die2 == 2 ) && ( die1 == 6 || die2 == 6 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Easy Eight!");
		}
		
		//3 and 4
		if(( die1 == 3 || die2 == 3 ) && ( die1 == 4 || die2 == 4 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Seven Out!");
		}
		
		
		//3 and 5
		if(( die1 == 3 || die2 == 3 ) && ( die1 == 5 || die2 == 5 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Easy Eight!");
		}
		
		//3 and 6
		if(( die1 == 3 || die2 == 3 ) && ( die1 == 6 || die2 == 6 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Nine!");
		}
		
		//4 and 5
		if(( die1 == 4 || die2 == 4 ) && ( die1 == 5 || die2 == 5 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Nine!");
		}
		
		
		//5 and 6
		if(( die1 == 5 || die2 == 5 ) && ( die1 == 6 || die2 == 6 ))
		{
			System.out.println("Die 1 is " + die1 + ". Die 2 is " + die2 + "." );
			System.out.println("Yo-leven!");
		}
    
  }
}
