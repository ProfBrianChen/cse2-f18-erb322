//Erik Bissell
//CSE002-110
//User inputs final price of meal, number of people, and % tip
//Check.java outputs how much each person needs to pay. 

//lets program get user input
import java.util.Scanner;
//start of class
public class Check {

  // start of main method
	public static void main(String[] args) {
	
    //creates a new scanner named 'scan'  
    Scanner scan = new Scanner(System.in);
    
    //prompts user for the cost of check
    System.out.print("Enter the original cost of the check in the form of xx.xx: ");
    
    //puts checkCost given by user into var checkCost
    double checkCost= scan.nextDouble();
    
    //prompts user for how much they want to tip
    System.out.println("Enter the percentage tip that you wish to pay as a whole number(in the form xx) : ");
    
    //gets the tip amount from user
    double tipPercent = scan.nextDouble();
    //changes tip percent into a decimal
    tipPercent /= 100;
    
    //prompts user for number of people and puts it into numPeople
    System.out.println("Enter the number of people who went out to dinner");
    int numPeople = scan.nextInt();
    
    double totalCost;
    double costPerPerson;
    //totalCosg is check + the tip
    totalCost = checkCost *(1+tipPercent);
    //costPerPerson is the total cost with tip evenly split between the amount of people
    costPerPerson = totalCost/numPeople;
    //prints out the cost of each person to 2 decimal points 
    System.out.printf("Each person in the group owes $ %.2f", costPerPerson);
    
    
	}// end of main method

}//end of class