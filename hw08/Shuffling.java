
import java.util.Random;
import java.util.Scanner;
public class Shuffling{ 
	
	public static void printArray(String[] list)
	{
		for(int i=0; i < list.length - 1; i++) 
		{	
			System.out.print(list[i] + " ");
			
		}
	}
	
	public static void shuffle(String [] list)
	{
		System.out.println(" ");
		System.out.println("Shuffled");
		System.out.println(" ");
		for (int i = 0; i<100; i++)
		{
		Random rand = new Random(); 
		
		int  swapIndex = rand.nextInt(51) + 1;
		String temp = list[0];
		list[0] = list[swapIndex];
		list[swapIndex] = temp; 
		}		
	}
					//array list , int index , int numCards
	public static String[] getHand(String[] list, int index, int numCards)
	{
		String [] hand = new String [numCards];
		System.out.println(" ");
		System.out.println("Hand");
		System.out.println(" ");
		if(numCards > index) 
		{
			// new deck of cards
			//not sure how to get a new deck of cards here 
			return hand; 
		}
		
		for(int i = 0; i< numCards; i++)
		{
			hand[i] = list[index-i];
			
		}
		return hand;		
	}
	
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13]; 
			//commented out to avoid printing list of cards twice
		//	System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 
		while(again == 1){ 
			hand = getHand(cards,index,numCards); 
			printArray(hand);
			index = index - numCards;
			System.out.println(" ");
			System.out.println("Enter a 1 if you want another hand drawn"); 
			again = scan.nextInt(); 
		}  
	} 
}
