//Erik Bissell
//CSE 002-110
//inputs side length
//ouputs a square with embedded X
import java.util.Scanner;

public class EncryptedX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//scanner named scan
		Scanner scan = new Scanner(System.in);
		
		System.out.println("how many lines of stars would you like");
		
		//validate that the input is an int
		int numStars = -1;
		boolean correctInt;
		do
		{
			//if it is an int, set numStars to that int
			correctInt = scan.hasNextInt();
			if (correctInt)
			{
				numStars = scan.nextInt();
			}
			//otherwise it gets 'cleared from conveyor belt'
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Enter the number of lines of stars you would like");
			}
      if(numStars < 0 || numStars > 100)
      {
        System.out.println("Not in range: please try again");
        correctInt = false;
      }  
      
		}while(!(correctInt));
		
		//nested for loop to make a grid
		for (int i = 0; i <= numStars; i++)
		{
			for( int j = 0; j <= numStars; j++)
			{
				//if statement allows the space to occur on both sides
				if((j == i) || (j == (numStars - (i))))
				{
					System.out.print(" ");
				}
				else
				{
					System.out.print("*");
				}
			}
			//new line
			System.out.println("");
		}
	}
}


