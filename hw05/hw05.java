//deals a 5 card hand x number of times
//inputs amount of hands 
//outputs rate at which 4 of a kind, 3 of a kind, two pair, and three pair occurs to 3 decimal
//Erik Bissell

import java.util.Scanner;
//start of class
public class hw05 {
    // start of main method
	public static void main(String[] args) {
    
    //creates a scanner named scan 
		Scanner scan = new Scanner(System.in);
    
		System.out.println("how many times would you like to be dealt a hand?");
		int numHands = scan.nextInt();
		
		int counter  = 0;
		
		//intialize number of each type of hand 
		//outside while loop bc need to access later
		int fourKindAmt = 0;
		int threeKindAmt = 0;
		int twoPairAmt = 0;
		int pairAmt= 0;
    
		//loop while counter is is less that number of hands that user wanted 
		while( counter < numHands)
		
		{
			
			int card1 = (int) (Math.random() * ((51)+ 1) +1);
			int card2 = (int) (Math.random() * ((51)+ 1) +1);
			int card3 = (int) (Math.random() * ((51)+ 1) +1);
			int card4 = (int) (Math.random() * ((51)+ 1) +1);
			int card5 = (int) (Math.random() * ((51)+ 1) +1);
			
			//keep looping to change value of cards that are the same
			while(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5 || card2 == card3 || card2 == card4 || card2 == card5 || card3 == card4 || card3 == card5 || card4 == card5)
			{
				if(card1 == card2)
				{	
					card2 = (int) (Math.random() * ((51)+ 1) +1);
				}
			
				if(card1 == card3)
				{
					card3 = (int) (Math.random() * ((51)+ 1) +1);	
				}
			
				if(card1 == card4)
				{
				 	card4 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card1 == card5)
				{
				 	card5 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card2 == card3)
				{
				 	card3 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card2 == card4)
				{
					card4 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card2 == card5)
				{
					card5 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card3 == card4)
				{
					card4 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card3 == card5)
				{
					card5 = (int) (Math.random() * ((51)+ 1) +1);
				}
				if(card4 == card5)
				{
					card5 = (int) (Math.random() * ((51)+ 1) +1);
				}
			}
				
			//change cards to their value
			int card1Value = card1%13;
			int card2Value = card2%13;
			int card3Value = card3%13;
			int card4Value = card4%13;
			int card5Value = card5%13;
			
			//check if for a 4 of a kind
			//continue if a 4 of a kind bc it will be a 3 of a kind and a 2 of a kind also
			//dont want to include that also
			if(card1Value == card2Value && card1Value == card3Value && card1Value == card4Value)
			{
				fourKindAmt ++;
				counter++;
				continue;
			} 
			else if(card1Value == card2Value && card1Value == card3Value && card1Value == card5Value)
			{
				fourKindAmt ++;
				counter++;
				continue;
			}
			else if(card1Value == card2Value && card1Value == card4Value && card1Value == card5Value)
			{
				fourKindAmt ++;
				counter++;
				continue;
			}
			else if(card1Value == card3Value && card1Value == card4Value && card1Value == card5Value)
			{
				fourKindAmt ++;
				counter++;
				continue;
			}
			
			//checks for 3 of a kind
			//continue bc it also account for a full house
			if(card1Value == card2Value && card1Value == card3Value)
			{
				threeKindAmt ++;
				if(card4Value == card5Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card1Value == card2Value && card1Value == card4Value)
			{
				threeKindAmt ++;
				if(card3Value == card5Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card1Value == card2Value && card1Value == card5Value)
			{
				threeKindAmt ++;
				if(card3Value == card4Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card1Value == card3Value && card1Value == card4Value)
			{
				threeKindAmt ++;
				if(card2Value == card5Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card1Value == card3Value && card1Value == card5Value)
			{
				threeKindAmt ++;
				if(card2Value == card4Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card1Value == card4Value && card1Value == card5Value)
			{
				threeKindAmt ++;
				if(card2Value == card3Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card2Value == card3Value && card2Value == card4Value)
			{
				threeKindAmt ++;
				if(card1Value == card5Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card2Value == card3Value && card2Value == card5Value)
			{
				threeKindAmt ++;
				if(card1Value == card4Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card2Value == card4Value && card2Value == card5Value)
			{
				threeKindAmt ++;
				if(card1Value == card3Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			else if(card3Value == card4Value && card3Value == card5Value)
			{
				threeKindAmt ++;
				if(card1Value == card2Value)
				{
					pairAmt ++;
				}
				counter++;
				continue;
			}
			
			//checks for a pair or a two pair 
			
			if(card1Value == card2Value)
			{
				if(card3Value == card4Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card3Value == card5Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card4Value == card5Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				
				pairAmt ++;
				counter++;
				continue;
			}
			
			if(card1Value == card3Value)
			{
				if(card2Value == card4Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card2Value == card5Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card4Value == card5Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				
				pairAmt ++;
				counter++;
				continue;
			}
			
			if(card1Value == card4Value)
			{
				if(card2Value == card3Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card2Value == card5Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card3Value == card5Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				
				pairAmt ++;
				counter++;
				continue;
			}
			
			if(card1Value == card5Value)
			{
				if(card2Value == card3Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card2Value == card4Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				else if(card3Value == card4Value)
				{
					twoPairAmt ++;
					counter++;
					continue;
				}
				
				pairAmt ++;
				counter++;
				continue;
			}
			
			//checks for remaining one pair option where a two pair is no longer an option
			
			if(card2Value == card3Value)
			{
				pairAmt++;
				counter++;
				continue;
			}
			else if(card2Value == card4Value)
			{
				pairAmt++;
				counter++;
				continue;
			}
			else if(card2Value == card5Value)
			{
				pairAmt++;
				counter++;
				continue;
			}
			else if(card3Value == card4Value)
			{
				pairAmt++;
				counter++;
				continue;
			}
			else if(card3Value == card5Value)
			{
				pairAmt++;
				counter++;
				continue;
			}
			else if(card4Value == card5Value)
			{
				pairAmt++;
				counter++;
				continue;
			}	
			
			counter++;
			
		}
		
    // declares an intializes rates for each type of hand 
		double fourKindRate = (double) fourKindAmt / (double) numHands;
		double threeKindRate = (double) threeKindAmt / (double) numHands;
		double twoPairRate = (double) twoPairAmt / (double) numHands;
		double pairRate = (double) pairAmt / (double) numHands;
		
    //prints out rates to 3 decimals 
		System.out.println("The number of loops: " + counter);
		System.out.printf(" The probability of a Four-of-a-kind: %.3f " , fourKindRate);
		System.out.println(" ");
		System.out.printf(" The probability of a Three-of-a-kind: %.3f " , threeKindRate);
		System.out.println(" ");
		System.out.printf(" The probability of a Two-pair: %.3f " , twoPairRate);
		System.out.println(" ");
		System.out.printf("The probability of a One-pair %.3f" , pairRate);
    
  }
}
