import java.util.Random;
import java.util.Scanner;

public class phase2 {
	
	public static String Adjectives()
	{
		Random randomGenerator = new Random();

		int  randoNumber = randomGenerator.nextInt(10);
		String adjective; 
		switch (randoNumber) {
        case 0:  adjective = "aloof";
                 break;
        case 1:  adjective = "skinny";
                 break;
        case 2:  adjective = "messy";
                 break;
        case 3:  adjective = "blue";
                 break;
        case 4:  adjective = "happy";
                 break;
        case 5:  adjective = "small";
                 break;
        case 6:  adjective = "big";
                 break;
        case 7:  adjective = "lopsided";
                 break;
        case 8:  adjective = "nifty";
                 break;
        case 9: adjective = "neat";
                 break;
        default: adjective = "error";
                 break;
    }
		
		return adjective;
		
	}
	
	public static String verb()
	{
		
		Random randomGenerator = new Random();

		int  randoNumber = randomGenerator.nextInt(10);
		String verb; 
		switch (randoNumber) {
        case 0:  verb = "covered";
                 break;
        case 1:  verb = "mugged";
                 break;
        case 2:  verb = "attacked";
                 break;
        case 3:  verb = "helped";
                 break;
        case 4:  verb = "arrested";
                 break;
        case 5:  verb = "hugged";
                 break;
        case 6:  verb = "exploded";
                 break;
        case 7:  verb = "fried";
                 break;
        case 8:  verb = "repaired";
                 break;
        case 9: verb = "saw";
                 break;
        default: verb = "error";
                 break;
		}
		return verb;
	}
	
	public static String nonPrimarySubject()
	{
		
		Random randomGenerator = new Random();

		int  randoNumber = randomGenerator.nextInt(10);
		String nonPrimarySubject; 
		switch (randoNumber) {
        case 0:  nonPrimarySubject = "frog";
                 break;
        case 1:  nonPrimarySubject = "rice";
                 break;
        case 2:  nonPrimarySubject = "orange";
                 break;
        case 3:  nonPrimarySubject = "tree";
                 break;
        case 4:  nonPrimarySubject = "flower";
                 break;
        case 5:  nonPrimarySubject = "sand";
                 break;
        case 6:  nonPrimarySubject = "volleyball";
                 break;
        case 7:  nonPrimarySubject = "coal";
                 break;
        case 8:  nonPrimarySubject = "crate";
                 break;
        case 9: nonPrimarySubject = "pen";
                 break;
        default: nonPrimarySubject = "error";
                 break;
		}
		return nonPrimarySubject;
	}
	
	public static String nonPrimaryObject()
	{
		
		Random randomGenerator = new Random();

		int  randoNumber = randomGenerator.nextInt(10);
		String nonPrimaryObject; 
		switch (randoNumber) {
        case 0:  nonPrimaryObject = "horse";
                 break;
        case 1:  nonPrimaryObject = "fish";
                 break;
        case 2:  nonPrimaryObject = "shark";
                 break;
        case 3:  nonPrimaryObject = "bear";
                 break;
        case 4:  nonPrimaryObject = "beast";
                 break;
        case 5:  nonPrimaryObject = "alpaca";
                 break;
        case 6:  nonPrimaryObject = "llama";
                 break;
        case 7:  nonPrimaryObject = "goat";
                 break;
        case 8:  nonPrimaryObject = "creature";
                 break;
        case 9: nonPrimaryObject = "enemy";
                 break;
        default: nonPrimaryObject = "error";
                 break;
		}
		return nonPrimaryObject;
	}
	
	public static String sentenceGenerator()
	{
		String subject = nonPrimarySubject();
		System.out.println("The " + Adjectives() + " " + Adjectives() + " " + subject + " " + verb() + " the " + Adjectives() + " " + nonPrimaryObject() );
		
		return subject;
	}
	
	public static String thesisGenerator(String input)
	{
		Random randomGenerator = new Random();
		int  randoNumber = randomGenerator.nextInt(2)+1;
		String subject = "error";
		if(randoNumber == 1)
		{
			subject = input;
		}
		else
		{
			subject = "It";
		}
		
		System.out.println(subject + " used" + " " + attackMethod() + " " + verb() + " " + nonPrimaryObject() + " at the " + Adjectives() + " "+ nonPrimarySubject());
		
    return input;
	}
	
	public static String conclusion(String input)
	{
		
		System.out.println("That " + input + " " + verb() + " its " + nonPrimaryObject());
    return " ";
	}
	
	public static String attackMethod()
	{
		
		Random randomGenerator = new Random();

		int  randoNumber = randomGenerator.nextInt(10);
		String nonPrimaryObject; 
		switch (randoNumber) {
        case 0:  nonPrimaryObject = "afterburners";
                 break;
        case 1:  nonPrimaryObject = "catapults";
                 break;
        case 2:  nonPrimaryObject = "the pope";
                 break;
        case 3:  nonPrimaryObject = "swords";
                 break;
        case 4:  nonPrimaryObject = "bombs";
                 break;
        case 5:  nonPrimaryObject = "bandanas";
                 break;
        case 6:  nonPrimaryObject = "bannanas";
                 break;
        case 7:  nonPrimaryObject = "iron bars";
                 break;
        case 8:  nonPrimaryObject = "mice";
                 break;
        case 9: nonPrimaryObject = "fingers";
                 break;
        default: nonPrimaryObject = "error";
                 break;
		}
		return nonPrimaryObject;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(conclusion(thesisGenerator(sentenceGenerator())));

		
		
		
	}

}
