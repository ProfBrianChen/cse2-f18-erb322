// Erik Bissell CSE 002 
//Homework 02
//manipulating data, simple calculations, printing numerical outputs
public class Arithmetic{

  public static void main(String args[]){
    
      
      //Number of pairs of pants
      int numPants = 3;
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of shirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
      //cost per belt
      double beltPrice = 33.99;

      //the tax rate
      double paSalesTax = 0.06;

      //total cost of pants- no tax
      double totalPricePants=(double)(numPants*pantsPrice);
      
      //total cost of shirts- no tax
      double totalPriceShirts=(double)(numShirts*shirtPrice);
    
      //total cost of belts- no tax
      double totalPriceBelt=(double)(numBelts*beltPrice);
      
      //sales tax for pants
      double salesTaxPants=totalPricePants* paSalesTax;
      
      //sales tax for shirts
      double salesTaxShirts= totalPriceShirts*paSalesTax;
      
      //sales tax for belt
      double salesTaxBelt= totalPriceBelt*paSalesTax;
    
      //total preTax cost of everything
      double totalPreTax= totalPriceShirts + totalPricePants +totalPriceBelt;
    
      //total sales tax of everything
      double totalSalesTax= salesTaxShirts+ salesTaxPants+salesTaxBelt;
    
      //total price of everything including tax
      double totalPostTax= totalPreTax +totalSalesTax;
    
      //prints out total cost of each item before tax
      //prints out total sales tax of each item
      //printf allows to round to 2 decimals
      System.out.printf("Total cost of shirts before tax:$ %.2f",totalPriceShirts);
      System.out.printf(". Sales tax is: $ %.2f",salesTaxShirts);
      System.out.println(" ");
      System.out.printf("Total cost of pants before tax: $ %.2f",totalPricePants);
      System.out.printf(". Sales tax is: $ %.2f",salesTaxPants);
      System.out.println(" ");
      System.out.printf("Total cost of belts before tax: $ %.2f",totalPriceBelt);
      System.out.printf(". Sales tax is: $ %.2f",salesTaxBelt);
      System.out.println(" ");
      System.out.printf("Total pre tax total is: $ %.2f",totalPreTax);
      System.out.println(" ");
      System.out.printf("Total sales tax is: $ %.2f",totalSalesTax);
      System.out.println(" ");
      System.out.printf("Total cost including sales tax: $ %.2f",totalPostTax);
    
    
    
  
  
    
    
      
    
      
      
      
      
    }
    
    
    
     
}