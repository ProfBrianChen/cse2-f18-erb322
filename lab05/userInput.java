//Erik Bissell
//Lab 5
//inputs course numbers, departmentname ,classes per week, class start time, professor name, and number of students
//checks to make sure proper type of inputs
//outputs the inputs in a clean sentence 


import java.util.Scanner;

public class userInput{
  public static void main(String[]args)
  {
 //initialize variables with values that would note an error later
		int courseNumber = -1;
		String departmentName = "error in department";
		int classesPerWeek = -1;
		String classStartTime = "error class time";
		String professorName = "error in professor";
		int numOfStudents = -1;
		
		//create a scanner to get input from user
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter the course Number for your class");
		
		//will hold the value of if user inputted a proper int
		boolean correctInt = false;
		
		do
		{
			//if it is an int, courseNumber gets set equal to that int
			correctInt = scan.hasNextInt();
			if (correctInt)
			{
				courseNumber = scan.nextInt();
			}
			//otherwise it gets 'cleared from conveyor belt'
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Please enter the course Number for your class");
			}
			//loops while the user hasnt put in a proper int 
		}while(!(correctInt));
		
		
		System.out.println("Now enter Department Name");
		
		//similar to correctInt, will hold value of if user inputted a string
		boolean correctString = false;
		do
		{
			//if it is a string, department name holds that value
			correctString = scan.hasNext();
			if (correctString)
			{
				departmentName = scan.next();
			}
			//otherewise clear that value
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Please enter the department name for your class");
			}
			//while the user hasnt inputted a string yet
		}while(!(correctString));
		
		System.out.println("Enter how many times each week that your class meets?");
		
		//change correctInt back to false
		correctInt = false;
		do
		{
			correctInt = scan.hasNextInt();
			if (correctInt)
			{
				classesPerWeek = scan.nextInt();
			}
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Please enter the number of times your class meets each week");
			}
		}while(!(correctInt));
		
		
		System.out.println("please enter the start time for your class");
		 correctString = false;
		do
		{
			correctString = scan.hasNext();
			if (correctString)
			{
				classStartTime = scan.next();
			}
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Please enter the start time for your class");
			}
		}while(!(correctString));
		
		
		System.out.println("Please enter your professor's name");
		correctString = false;
		do
		{
			correctString = scan.hasNext();
			if (correctString)
			{
				professorName = scan.next();
			}
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Please enter your professor's name");
			}
		}while(!(correctString));
		
		
		System.out.println("please enter number of students in your class");
		correctInt = false;
		do
		{
			correctInt = scan.hasNextInt();
			if (correctInt)
			{
				numOfStudents = scan.nextInt();
			}
			else
			{
				scan.next();
				System.out.println("Try again.");
				System.out.println("Please enter the number of students in your class");
			}
		}while(!(correctInt));
		
		
		
		System.out.println("you are in " +  departmentName + courseNumber);
		System.out.println("You and " + numOfStudents + " other people meet with " + professorName + " " + classesPerWeek + " at " + classStartTime);
		
		
  }
}