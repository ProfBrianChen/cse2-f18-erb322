
// Erik Bissell CSE002-316

// Cyclometer takes the time, rotations of wheel, and diameter wheel for 2 trips
// using PI, feetPerMile, inchesPerFoot, secondsPerMinute 
//it outputs distance in miles for each trip and the total distance
//outputs time in minutes for each trip



public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      //
      int secsTrip1=480;  // how long trip 1  in seconds 
      int secsTrip2=3220;  // how long trip 2 in seconds
		  int countsTrip1=1561;  // how many rotations wheel makes in trip 1
		  int countsTrip2=9037; // how many rotations wheel makes in trip 2

      double wheelDiameter=27.0;
      double PI = 3.14159;
      int feetPerMile=5280;
      int inchesPerFoot=12;
      int secondsPerMinute=60;
      double distanceTrip1, distanceTrip2, totalDistance;
      
      //cast (secsTrip/secondsPerMinute) as a double in order to print a double amount of minutes and not an int
      System.out.println("Trip 1 took "+(double) secsTrip1/secondsPerMinute + " minutes and had "+ countsTrip1 + " counts."); 
      System.out.println("Trip 2 took "+(double) secsTrip2/secondsPerMinute + " minutes and had "+ countsTrip2 + " counts.");
      
      
      
      distanceTrip1=countsTrip1*wheelDiameter*PI;   // calculating how far trip1 was in inches - rotations * circumference of wheel
                                                    // circumference= PI * diameter
      
      distanceTrip2=countsTrip2*wheelDiameter*PI;   // same as distanceTrip1 but for trip 2
      
      distanceTrip1= (distanceTrip1/inchesPerFoot)/feetPerMile; //gets distanceTrip1 in miles
      distanceTrip2= (distanceTrip2/inchesPerFoot)/feetPerMile; //gets distanceTrip2 in miles
      totalDistance= distanceTrip1 + distanceTrip2; // calculates total distance in miles
      
      System.out.println("Trip 1 was "+ distanceTrip1 + " miles");
      System.out.println("Trip 2 was "+ distanceTrip2 + " miles");
      System.out.println("The total distance was " + totalDistance + " miles.");
      

      
      
	}  //end of main method   
} //end of class

