//Erik Bissell
//CSE002-110
// gets amount of rainfall in an area
//outputs cubic miles of rain 

//lets program get user input
import java.util.Scanner;
//start of class
public class Convert {

  // start of main method
	public static void main(String[] args) {
    
    double affectedArea;
    double rainFall;
    
    //creates a scanner named scan
    Scanner scan = new Scanner(System.in);
    
    //user inputs the affected area of the hurricane in acres as a double
    System.out.println("Enter the affected area in acres: ");
    affectedArea = scan.nextDouble();
    
    //user inputs the total rainfall in inches as a double
    System.out.println("Enter the rainfall in the affected area in inches: ");
    rainFall = scan.nextDouble();
    
   //number of acres in each mile
   final double ACRES_PER_MILE = 640;
   
   //number of inches per mile
   final double INCHES_PER_MILE = 63360;
    
   //affected area in sq miles
   double affectedMilesSquared = affectedArea / ACRES_PER_MILE;
   
   //rain fall in miles instead of inches
   double milesRainfall = rainFall / INCHES_PER_MILE; 
    
   //cubic miles of rainfall in affected area 
   double cubicMiles = affectedMilesSquared * milesRainfall;
    
   System.out.println(cubicMiles + " cubic miles of rain in area affected");
    

    
    
    
    
    
  }
}
