
public class CardGenerator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		   // randomly chooses a card from deck 1-52
		   int card = (int)(Math.random()*(52))+1; 
		   
			 // creates a variable of card number
			 // if it keeps value of 0 - know there is an error
		    int cardNumber= 0;
		    
		    //if the card is a multiple of 13 it is a king
		    if(card == 13 || card == 26 || card == 39 || card == 52)
		    		{
		    			cardNumber = 13;
		    		}
		    //otherwise use modulo to figure out what type of card it is 
		    else
		    {
		    	cardNumber = card % 13;
		    }
		    //some special cases
		    //13 is king
		    if(cardNumber == 13)
		    {
		      System.out.print("You picked the King of ");
		    }
		    //12 is queen 
		    else if(cardNumber==12)
		    {
		      System.out.print("You picked the Queen of ");
		    }
		    //11 is jack 
		     else if(cardNumber==11)
		    {
		      System.out.print("You picked the Jack of ");
		    }
		    //1 is ace
		     else if(cardNumber==1)
		    {
		      System.out.print("You picked the Ace of ");
		    }
		    //otherwise the cardNumber is the value that it holds
		    else
		    {
		      System.out.print("You picked the " + cardNumber + " of ");
		    }
    
		    //determines what suit card is
		    //1-13 is diamonds
		    if(card >= 1 && card <= 13)
		    {
		      System.out.println("Diamonds");
		    }
    
		    //14-26 is Clubs
		    else if(card >= 14 && card <= 26)
		    {
		      System.out.println("Clubs");
		    }
    
		    //27-39 is hearts
		    else if(card >= 27 && card <= 39)
		    {
		      System.out.println("Hearts");
		    }
    
		    //40-52 is spades
		    else if(card >= 40 && card <= 52)
		    {
		      System.out.println("Spades");
		    }
		    
		    System.out.println("");
		    System.out.println(card);
		    
		    
		
		
		
		
		

	}

}